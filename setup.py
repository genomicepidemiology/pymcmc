#!/usr/bin/env python
''' Setup file for pyMCMC
'''

from distutils.core import setup

setup(name='pyMCMC',
      version='0.1',
      description='Markov Chain Monte Carlo based model for Poisson distributed data',
      author='Christian Brinch',
      author_email='cbri@food.dtu.dk',
      packages=['pymcmc'],
      install_requires=['matplotlib>=3.3.3', 'numpy>=1.18.5', 'pandas>=1.2.4',
                        'pymc3>=3.9.3', 'scipy>=1.6.0'],
      )

