'''
    pyMCMC init script
'''

__title__ = "pyMCMC"
__author__ = "Christian Brinch"
__email__ = "cbri@food.dtu.dk"
__copyright__ = "Copyright 2021 C. Brinch"
__version__ = 0.1
__all__ = ['pymcmc']

from .pymcmc import *
