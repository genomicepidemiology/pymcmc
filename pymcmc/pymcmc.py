# -*- coding: utf-8 -*-
''' This script is used to calculate MCMC models of Poisson distributed data.
    Christian Brinch, 2021
'''

import pickle
from scipy import signal
import pandas as pd
import numpy as np
import pymc3 as pm
import matplotlib.dates as mdates
import matplotlib.ticker as ticker
import matplotlib.pyplot as plt
import matplotlib as mpl

mpl.rcParams['figure.dpi'] = 150


class DataSet():
    ''' A class to hold one data set '''

    def __init__(self, data, rerun=False):
        self.data = data
        self.slice = slice(-1)
        #self.data['Date'] = pd.to_datetime(self.data['Dato'], format="%d/%m/%Y")
        self.days = np.linspace(0, len(self.data)-1, len(self.data))
        self.model = {}
        if rerun:
            for col in self.data:
                self.model[col] = self.pymc3model(col)

            with open('model.pickle', 'wb') as file:
                pickle.dump(self.model, file)
        else:
            with open('model.pickle', "rb") as infile:
                self.model = pickle.load(infile)

    def pymc3model(self, col):
        ''' The actual probabilistic model from Marco Puts '''
        with pm.Model() as model:
            alpha = pm.Gamma("alpha", alpha=2, beta=0.1)
            tau_loglambda = pm.Gamma("tau_loglambda", alpha=20, beta=0.1)
            loglambda = pm.AR("loglambda",
                              rho=[0.999],
                              tau=tau_loglambda,
                              shape=(self.data[col].shape[0],)
                              )
            rate = pm.Deterministic("rate", np.exp(loglambda))
            _ = pm.NegativeBinomial("observed", mu=rate, alpha=alpha, observed=self.data[col])

        with model:
            # trace = pm.sample(200, tune=4000, chains=8, cores=8)
            trace = pm.sample(20, tune=100, chains=2, cores=1)
        return trace

    def avg(self, col):
        return self.model[col]['rate'].mean(axis=0)[self.slice]

    def std(self, col):
        return self.model[col]['rate'].std(axis=0)[self.slice]

    def diff(self, col):
        ''' Calculate the derivative '''
        return np.diff(self.avg(col))

    def hannfilter(self, col):
        ''' Hann filer '''
        win = signal.hann(14)
        return signal.convolve(self.diff(col), win, mode='same') / sum(win)

    def plotdata(self, axis, col, color):
        ''' Plot the data as bars '''
        axis.bar(self.data.index[self.slice],
                 self.data[col].iloc[self.slice], color=color, alpha=0.7)
        lslice = int(len(self.data[self.slice])/10)
        new_tick_locations = np.array([i for i in self.data[self.slice].index[::lslice]])
        axis.set_xticks(new_tick_locations)
        #dates = [self.data.index[0] + np.timedelta64(i, 'D') for i in new_tick_locations]
        #dates = [date.month_name()[:3]+" "+str(date.year) for date in dates]
        # axis.set_xticklabels(dates)
        plt.xticks(rotation=45)
        axis.set_xlabel('Date')
        axis.set_ylabel('Rate')

    def plotmodel(self, axis, col, color):
        ''' Plot the average model '''
        axis.plot(self.data.index[self.slice], self.avg(col), color=color, lw=2)
        axis.fill_between(self.data.index[self.slice], self.avg(col) -
                          self.std(col), self.avg(col) + self.std(col), color=color, alpha=0.4)

    def plotderivative(self, axis, col, color):
        ''' Plot the thime derivative '''

        if col == 'Deaths':
            axis.plot(self.data.index[self.slice][:-1], 5*self.hannfilter(
                col), color=color, lw=2, alpha=0.8)
        else:
            axis.plot(self.data.index[self.slice][:-1], self.hannfilter(
                col), color=color, lw=2, alpha=0.8)

        lslice = int(len(self.data[self.slice])/10)
        new_tick_locations = np.array([i for i in self.data[self.slice].index[::lslice]])
        axis.set_xticks(new_tick_locations)
        dates = [self.data['Dato'][0] + np.timedelta64(i, 'D') for i in new_tick_locations]
        dates = [date.month_name()[:3]+" "+str(date.year) for date in dates]
        axis.set_xticklabels(dates)
        plt.xticks(rotation=45)
        axis.set_xlabel('Date')
        axis.set_ylabel('$\partial_t$ Rate')
        axis.plot(self.data.index[self.slice], len(
            self.data.index[self.slice])*[0], '--', color='black', lw=0.6)
